module Spree::ProductDecorator
  MAX_NUMBER_OF_RELATED_PRODUCTS = 4
  def related_products
    taxons.sample.products.includes(master: [:default_price, :images]).where.not(id: id).limit(MAX_NUMBER_OF_RELATED_PRODUCTS)
  end
  Spree::Product.prepend self
end
