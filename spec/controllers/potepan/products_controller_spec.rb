require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let(:product) { create(:product, taxons: [taxon]) }
    let(:taxon) { create(:taxon) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before { get :show, params: { id: product.id } }

    it 'assigns @product' do
      expect(assigns(:product)).to eq product
    end

    it 'has a 200 status code' do
      expect(response).to have_http_status 200
    end

    it 'renders the :show template' do
      expect(response).to render_template :show
    end

    it "number of related_products is 4 or less" do
      expect(assigns(:related_products).size).to be <= 4
    end

    it "Do not include product details in related products" do
      expect(product.related_products).not_to include product
    end
  end
end
