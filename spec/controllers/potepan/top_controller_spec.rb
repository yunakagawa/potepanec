require 'rails_helper'

RSpec.describe Potepan::TopController, type: :controller do
  describe "#index" do
    let!(:products) { create_list(:product, 9) }

    before { get :index }

    it 'assigns @products' do
      expect(assigns(:products)).to match_array(products)
    end

    it 'has a 200 status code' do
      expect(response).to have_http_status 200
    end

    it 'renders the :index template' do
      expect(response).to render_template :index
    end

    it "number of new_products is 8 or less" do
      expect(assigns(:new_products).size).to be <= 8
    end
  end
end
