require 'rails_helper'

RSpec.feature 'Categories', type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  background do
    visit potepan_category_path(id: taxon.id)
  end

  scenario 'User visits product detail page' do
    expect(page).to have_link product.name
    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end

  scenario 'Expected information is displayed on the category page' do
    within '.productBox' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
    end
  end
end
